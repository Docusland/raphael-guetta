var game = new Phaser.Game(640, 400, Phaser.CANVAS, 'story', { preload: preload, create: create, update: update, render: render });
var graphics;
var tween;
var messageH;
var finaltext;


var state = "init";

function preload() {
  game.load.spritesheet('voiture', 'media/voiture.svg', 256, 128);
  game.load.spritesheet('rafi', 'media/rafi_story.png', 320, 320);

  game.load.spritesheet('customer', 'media/customer.png', 320, 320);
  game.load.spritesheet('phone', 'media/telephoneRing.png', 64, 64);
  game.load.image('rafiH', 'media/rafiHead2.png');
  game.load.image('customerH', 'media/marleHead.png');
  game.load.image('computer', 'media/computer.png');
  game.load.image('background', 'media/bg_city.png');
  game.load.image('backgroundabove', 'media/bg_city2.png');
  game.load.image('customerRoom', 'media/scen03.png');
  game.load.image('customerRoomFish', 'media/customerRoomFish.png');
  game.load.image('callBackground', 'media/scen01.png');
  game.load.spritesheet('coin', 'media/coin.png', 32, 32);
  game.load.spritesheet('music', 'media/music.png', 64, 64);
  game.load.image('carte', 'media/carte.png');
}

function clearTalk() {
  if (graphics) graphics.clear();

  if (messageH) {
    messageH.message.kill();
    messageH.kill();
  }

}
function displayTalk(spriteName, message, keepGraphics) {
  if (!keepGraphics) {
    clearTalk();
  }
  graphics = game.add.graphics(0, 0);

  graphics.beginFill('#ff0000');
  var rect = graphics.drawRect(0, 260, 640, 140);
  graphics.endFill('#ff0000');

  messageH = game.add.sprite(100, 270, spriteName);
  messageH.width = 100;
  messageH.height = 100; 

  messageH.message = game.add.text(250, 280, message, { font: "16pt Courier", fill: "#fff", stroke: "#c0c0c0", strokeThickness: 2, wordWrap: "true", wordWrapWidth: "360" });

}
function create() {
  game.stage.backgroundColor = '#000';

  bg = game.add.image(0, -88, 'background');


  leftScen1 = game.add.sprite(0, 0, 'customerRoomFish');
  scen1 = game.add.sprite(640, 0, 'callBackground');


  customerRoom = game.add.sprite(0, 0, 'customerRoom');
  customerRoom.alpha = 0;

  customer = game.add.sprite(500, 150, 'customer');
  customer.anchor.set(0.5);
  customer.scale.x = -0.3;
  customer.scale.y = 0.3;


  music = game.add.sprite(570, 120, 'music');
  music.anchor.set(1, 0.5);
  music.animations.add('stop', [0], 1, false);
  music.animations.add('play', [0, 1, 2], 6, true).play();

  customer.scale.x = -0.6;
  customer.scale.y = 0.6;
  customer.y = 200;
  customer.x = 510;

  customer.alpha = 0;

  customer.talk = function (message, keepGraphics) {
    displayTalk('customerH', message);
  };

  rafi = game.add.sprite(150, 220, 'rafi');
  rafi.anchor.set(0.5, 0.5);
  rafi.scale.x = 0.6;
  rafi.scale.y = 0.6;
  rafi.animations.add('stop', [0], 1, false);
  rafi.animations.add('walk', [1, 2, 3, 2], 8, true);
  rafi.animations.add('turnBack', [5], 1, false);
  rafi.animations.add('teach', [4], 1, false);
  var bpmText;
  rafi.talk = function (message, keepGraphics) {
    displayTalk('rafiH', message);
  };


  bg2 = game.add.image(0, -88, 'backgroundabove');
  bg2.alpha = 0;

  voitureY = 200;
  voiture = game.add.sprite(400, voitureY, 'voiture');
  voiture.animations.add('start', [0, 1, 2], 1, true);
  voiture.animations.add('stop', [0], 1, false);
  voiture.anchor.set(0.5);
  voiture.scale.x = -1.5;
  voiture.scale.y = 1.5;
  voiture.alpha = 0;

  computer = game.add.sprite(450, 150, 'computer');
  computer.anchor.set(0.5);
  computer.scale.x = -0.5;
  computer.scale.y = 0.5;
  computer.tint = 0xff0000;
  computer.alpha = 0;


  graphicsTelephone = game.add.graphics(0, 0);
  graphicsTelephone.beginFill("#000");
  graphicsTelephone.drawRect(600, 0, 240, 400);
  graphicsTelephone.endFill();

  phone = game.add.sprite(680, 100, 'phone');

  phone.animations.add("ring", [0, 1, 0, 1], 4, true);
  phone.animations.add("stop", [0], 1, false);

  game.world.setBounds(0, 0, 1000, 388);

  onTweenComplete();

}

function onTweenComplete() {
  switch (state) {

    case 'init' :
      rafi.animations.play("walk");
      tween = game.add.tween(rafi).to({ x: 500}, 2000, "Linear", true);
      tween.onComplete.add(function () {
        rafi.animations.play("stop");

        tween1 = game.add.tween(rafi).to({ x: 500}, 1000, "Linear", true);
        tween1.onComplete.add(function () {
          rafi.animations.play("turnBack");
          game.time.events.add(Phaser.Timer.SECOND * 1, function () {
            rafi.animations.play("stop");
            state = "phoneRings";
            onTweenComplete()
          }, this);
        }, this);
      }, this);
      break;
    case 'phoneRings':
      var RECUL = 400;

      phone.animations.play("ring");
      music.animations.play("stop");
      game.add.tween(rafi).to({ x: rafi.x - RECUL}, 1000, "Linear", true);
      game.add.tween(leftScen1).to({ x: leftScen1.x - RECUL}, 1000, "Linear", true);
      game.add.tween(graphicsTelephone).to({ x: graphicsTelephone.x - RECUL}, 1000, "Linear", true);
      game.add.tween(phone).to({ x: phone.x - RECUL}, 1000, "Linear", true);
      game.add.tween(music).to({ x: music.x - RECUL}, 1000, "Linear", true);

      game.time.events.add(Phaser.Timer.SECOND * 2, function () {
        customer.alpha = 1;
        rafi.talk("Allô ?", true);
        phone.animations.play("stop");
      }, this);
      game.time.events.add(Phaser.Timer.SECOND * 5, function () {
        game.add.tween(graphics).to({alpha: 0}, 1000, "Linear", true);
        customer.talk("Bonjour !");
      }, this);
      game.time.events.add(Phaser.Timer.SECOND * 8, function () {
        customer.talk("J'ai un problème sur mon ordinateur !")
      }, this);
      game.time.events.add(Phaser.Timer.SECOND * 11, function () {
        customer.talk("Pourriez-vous m'aider ?")
      }, this);
      game.time.events.add(Phaser.Timer.SECOND * 14, function () {
        rafi.talk("Bien sûr ! J'arrive !")
      }, this);
      state = "startScenario";
      game.time.events.add(Phaser.Timer.SECOND * 17, function () {
        rafi.scale.x *= -1;
        rafi.x = 50;
        rafi.animations.play("walk");
        game.add.tween(rafi).to({x: rafi.x - 225}, 500, "Linear", true).onComplete.add(onTweenComplete, this);
      }, this);

      break;

    case "startScenario":
      leftScen1.kill();
      graphicsTelephone.clear();
      clearTalk();
      rafi.x = 200;
      rafi.y = 150;
      rafi.scale.x = 0.3;
      rafi.scale.y = 0.3;

      customer.alpha = 0;
      phone.alpha = 0;

      scen1.alpha = 0;
      bg2.alpha = 1;
      voiture.alpha = 1;
      rafi.animations.play('walk');
      tween = game.add.tween(rafi).to({ x: 440 }, 1200, "Linear", true);
      state = "walk";
      tween.onComplete.add(onTweenComplete, this);
      break;

    case "walk":
      state = "rafJoinedCar";
      rafi.animations.play("stop");
      tween = game.add.tween(rafi).to({ alpha: 0 }, 1000, "Linear", true);
      tween.onComplete.add(onTweenComplete, this);
      break;

    case "rafJoinedCar":
      rafi.animations.stop();
      voiture.animations.play('start');

      tween = game.add.tween(voiture).to({ x: 1200 }, 2500, Phaser.Easing.Cubic.In, true);
      game.add.tween(game.camera).to({ x: 500 }, 2000, "Linear", true);
      tween.onComplete.add(onTweenComplete, this);

      state = "carLeftScreenStep1";
      break;

    case "carLeftScreenStep1":
      graphics.beginFill('#000');
      graphics.drawRect(0, 0, 1000, 400);
      graphics.alpha = 0;
      graphics.endFill();
      tween = game.add.tween(graphics).to({ alpha: 1 }, 1000, "Linear", true);
      state = "carLeftScreenStep2";
      voiture.x = -200;
      voiture.alpha = 0;
      tween.onComplete.add(onTweenComplete);

      break;

    case "carLeftScreenStep2":
      tween = game.add.tween(graphics).to({ alpha: 0 }, 1000, "Linear", true);
      game.camera.x = 0;
      state = "carLeftScreenStep3";
      tween.onComplete.add(onTweenComplete);
      break;

    case "carLeftScreenStep3":
      voiture.anchor.setTo(.5, 1);
      voiture.scale.x = -1.5;
      voitureY = voitureY + 100;
      voiture.y = voitureY;
      voiture.alpha = 1;
      tween = game.add.tween(voiture).to({ x: 600 }, 1600, Phaser.Easing.Cubic.Out, true);
      game.time.events.add(500, function () {
        game.add.tween(game.camera).to({ x: 700 }, 1400, "Linear", true);
      }, this);

      voiture.alpha = 1;
      tween.onComplete.add(function(){
        state = "carArrived";
      }, this);
      tween.onComplete.add(onTweenComplete, this);
      break;

    case "carArrived":
      voiture.animations.stop();
      voiture.animations.play("stop");
      rafi.x = voiture.x;
      rafi.y = voiture.y - 100;
      bg2.alpha = 0;
      rafi.scale.x *= -1;
      rafi.animations.play("stop");
      tween = game.add.tween(rafi).to({ alpha: 1 }, 500, "Linear", true);
      state = "rafReadyToJoinComputer";
      tween.onComplete.add(onTweenComplete, this);
      break;

    case  "rafReadyToJoinComputer":
      rafi.animations.play("walk");
      game.time.events.add(500, function () {
        tween = game.add.tween(rafi).to({ x: 500, y: 170 }, 1500, "Linear", true);
        game.add.tween(rafi.scale).to({ x: -0.2, y: 0.2 }, 1500, "Linear", true);
        tween.onComplete.add(onTweenComplete, this);
      }, this);

      state = "rafiEnterHouse";
      break;

    case "rafiEnterHouse":
      rafi.animations.play('turnBack');
      graphics = game.add.graphics(0, 0);
      graphics.beginFill('#000');
      graphics.drawRect(0, 0, 1000, 400);
      graphics.endFill();
      graphics.alpha = 0;

      state = "rafiEnteredHouse";

      tween = game.add.tween(graphics).to({ alpha: 1 }, 1000, "Linear", true);
      tween.onComplete.add(onTweenComplete, this);
      break;

    case "rafiEnteredHouse":
      game.camera.x -= 350;

      customerRoom.x = game.camera.x;
      customerRoom.y = game.camera.y - 240;
      customerRoom.alpha = 1;
      customer.alpha = 1;

      customer.scale.x = -0.3;
      customer.scale.y = 0.3;
      customer.x = 500;
      customer.y = 170;

      computer.alpha = 1;
      rafi.animations.stop();
      rafi.animations.play("stop");
      rafi.scale.x *= -1.5;
      rafi.scale.y *= 1.5;
      rafi.x = 350;
      rafi.y = 260;
      rafi.alpha = 1;
      voiture.alpha = 0;

      tween = game.add.tween(graphics).to({ alpha: 0 }, 1000, "Linear", true);
      game.time.events.add(Phaser.Timer.SECOND * 1, function () {
        rafi.talk("Bonjour !");
      }, this);
      game.time.events.add(Phaser.Timer.SECOND * 3, function () {
        customer.talk("Bonjour !");
      }, this);
      game.time.events.add(Phaser.Timer.SECOND * 5, function () {
        rafi.talk("Vous m'avez appelé pour réparer votre ordinateur ?");
      }, this);
      game.time.events.add(Phaser.Timer.SECOND * 8, function () {
        customer.talk("Ah oui... Le voilà, il me fait tourner en bourrique !");
      }, this);
      game.time.events.add(Phaser.Timer.SECOND * 12, function () {
        rafi.talk("Je vais regarder ça...");
      }, this);
      game.time.events.add(Phaser.Timer.SECOND * 13, function () {
        state = "rafMovesToComputer";
        onTweenComplete();
      }, this);

      break;

    case "rafMovesToComputer":
      rafi.animations.play("walk");
      tween = game.add.tween(rafi).to({ y: 150, x: 400}, 1000, "Linear", true);
      state = "rafRepairsComputer";
      tween.onComplete.add(onTweenComplete, this);
      break;

    case "rafRepairsComputer":
      rafi.animations.play("stop");
      tween = game.add.tween(computer).to({ tint: 0xFFFFFF }, 6000, "Linear", true);

      game.time.events.add(Phaser.Timer.SECOND * 2, function () {
        rafi.talk("Alors...")
      }, this);

      game.time.events.add(Phaser.Timer.SECOND * 4, function () {
        rafi.talk("Ah, je pense avoir trouvé !")
      }, this);
      state = "rafHasFixedComputer";

      tween.onComplete.add(onTweenComplete, this);
      break;

    case "rafHasFixedComputer":
      rafi.talk("C'est bon ! Je vous explique...");
      rafi.animations.play("teach");
      game.time.events.add(Phaser.Timer.SECOND * 3, function () {
        rafi.animations.play("stop");
        customer.talk("Merci beaucoup ! Je vous dois combien ?")
      }, this);
      game.time.events.add(Phaser.Timer.SECOND * 6, function () {
        rafi.talk("Mon tarif est de 40€ par heure d'intervention.")
      }, this);
      game.time.events.add(Phaser.Timer.SECOND * 9, function () {
        customer.talk("Vous me sauvez ma journée !")
      }, this);
      game.time.events.add(Phaser.Timer.SECOND * 10, function () {
        var coin = game.add.sprite(rafi.x - 16, rafi.y - rafi.height / 2 - 70, 'coin');
        coin.animations.add("roll", [0, 1, 2, 3, 4, 5, 6, 7], 40, true).play();
        coin.scale.x = 0.7;
        coin.scale.y = 0.7;
        var tw = game.add.tween(coin).to({y: rafi.y - rafi.height / 2}, 1000, "Linear", true);
        tw.onComplete.add(function () {
          coin.kill();
        });
      }, this);
      game.time.events.add(Phaser.Timer.SECOND * 13, function () {
          state = "rafExplainedToCustomer";
          rafi.animations.play("walk");
          rafi.scale.x *= -1;
          tween = game.add.tween(rafi).to({ x: 330, y: 260}, 1000, Phaser.Easing.Sinusoidal.InOut, true);
          tween.onComplete.add(onTweenComplete, this);
        },
        this);

      break;

    case "rafExplainedToCustomer":
      rafi.animations.play("stop");
      rafi.talk("Merci, et à bientôt !");
      game.time.events.add(Phaser.Timer.SECOND * 2, function () {
        state = "readyToLeave";
        onTweenComplete();
      }, this);

      break;
    case "readyToLeave":
      clearTalk();
      bg.alpha = 0;
      game.add.tween(customerRoom).to({ alpha: 0 }, 1000, "Linear", true);
      game.add.tween(customer).to({ alpha: 0 }, 1000, "Linear", true);

      game.add.tween(computer).to({ alpha: 0 }, 1000, "Linear", true);

      tween = game.add.tween(rafi).to({ alpha: 0 }, 1000, "Linear", true);
      tween.onComplete.add(onTweenComplete, this);
      state = "TheEnd";

      break;

    case "TheEnd":
      customerRoom.alpha = 0;
      bg.alpha = 0;
      graphics.beginFill('#000');
      graphics.drawRect(50, 50, 540, 300);
      graphics.beginFill('#fff');
      graphics.drawRect(54, 54, 532, 292);
      graphics.endFill();
      var style = { font: "30px Arial", fill: "#000000", align: "center" };

      var carte = game.add.sprite(game.camera.x + 119, game.camera.y + 194, 'carte'); //402 212
      carte.anchor.set(0.5);
      carte.x = game.camera.x + 320;
      carte.y = game.camera.y + 200;
      break;
    default:
      throw new Error("unknown state");
      break;


  }
}
function update() {
}
function render() {
  if (['carLeftScreenStep1', 'carLeftScreenStep2', 'carLeftScreenStep3'].indexOf(state) > -1) {
    voiture.y = voitureY + Math.round(Math.sin(game.time.now / 10) * 2);
  }
}































































