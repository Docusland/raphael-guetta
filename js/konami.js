var body = document.getElementsByTagName("body")[0];
var containerDiv = document.getElementsByClassName("konamiContainer")[0];

body.onkeydown = konami;

let counter = 0;
let lastKey;

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}
async function displayKey(code) {
  console.log("Enterring displayKey");
  var element = document.createElement("img");
  element.src = "media/konami/keyboard-key-" + code + ".svg";
  element.classList.add("key");
  containerDiv.appendChild(element);
  await sleep(50);
  element.classList.add("appear");
  console.log("appearing child");
}
function displayHUD(key) {
  document.location = "http://localhost:1234/xedni.html";
}

async function konami(e) {
  console.log("Enterring konami");
  let key = e.code;
  console.log("Found key" + key);
  let keyMap = [
    "ArrowUp",
    "ArrowUp",
    "ArrowDown",
    "ArrowDown",
    "ArrowLeft",
    "ArrowRight",
    "ArrowLeft",
    "ArrowRight",
    "KeyB",
    "KeyQ",
  ];
  let failed = false;
  if (key != lastKey) {
    lastKey = key;
    keyMap[counter] != key ? (failed = true) : "";
  }
  if (!failed) {
    counter++;
    displayKey(key);
  } else {
    counter = 0;

    [].forEach.call(containerDiv.querySelectorAll("img"), function (el) {
      el.classList.remove("appear");
    });
    await sleep(1000);
    [].forEach.call(containerDiv.querySelectorAll("img"), function (el) {
      containerDiv.removeChild(el);
    });
  }
  counter == keyMap.length ? displayHUD() : "";
}
