var game = new Phaser.Game(640, 400, Phaser.CANVAS, "story", {
  preload: preload,
  create: create,
  update: update,
  render: render,
});

/**
 * Used in order to launch a new tab, opening isaac in SWF.
 */
const isaac_game = {
  window: null,
  isPlaying() {
    return this.window != null && !this.window.closed
  },
  play() {
    this.window = window.open('./playing-isaac.html');
  }
}
const states = {
  init: 'INIT',
  playing_isaac: 'PLAYINGISAAC'
}
let state = states.init;

let player;
function initialize_player() {
  player = {
    isJumping: false,
    sprite: rafi,
    animations: {
      walk: 'walk',
      turnBack: 'turnBack',
    },
    move() {
      if (state == states.init) {
        if (keys.leftKey.isDown || keys.rightKey.isDown) {
          if (this.sprite.animations.name != this.animations.walk) {
            this.sprite.animations.stop();
            this.sprite.animations.play(this.animations.walk);
          }
          var dir = keys.leftKey.isDown ? -1 : 1;
          this.sprite.scale.x = dir * 0.6;
          this.sprite.x = this.sprite.x + dir * 10;
        }

        if (keys.spaceKey.isDown && !this.isJumping) {
          this.isJumping = true;
          var initial_pos = this.sprite.y;
          var up = game.add.tween(this.sprite).to({ y: initial_pos - 100 }, 200, "Linear", true);
          var down = game.add.tween(this.sprite).to({ y: initial_pos }, 200, "Linear", true);
          up.chain(down);
          console.log('Jumping');
          up.start();
        }
      }
    }
  }
}
var keys = {
  init: function () {
    this.leftKey = game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
    this.rightKey = game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
    this.spaceKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
    this.upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
    this.downKey = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
  },
};

function preload() {
  game.load.spritesheet("rafi", "media/rafi_story.png", 320, 320);
  game.load.image("customerRoomFish", "media/customerRoomFish.png");
  game.load.spritesheet("music", "media/music.png", 64, 64);
  game.load.audio("piano_music", ["media/piano.mp3", "media/piano.ogg"]);
}
function create() {
  game.stage.backgroundColor = "#000";
  leftScen1 = game.add.sprite(0, 0, "customerRoomFish");

  music = game.add.sprite(570, 120, "music");
  music.anchor.set(1, 0.5);
  music.animations.add("stop", [0], 1, false);
  music.animations.add("play", [0, 1, 2], 6, true).play();

  rafi = game.add.sprite(150, 220, "rafi");
  rafi.anchor.set(0.5, 0.5);
  rafi.scale.x = 0.6;
  rafi.scale.y = 0.6;
  rafi.animations.add("stop", [0], 1, false);
  rafi.animations.add("walk", [1, 2, 3, 2], 8, true);
  rafi.animations.add("turnBack", [5], 1, false);
  rafi.animations.add("teach", [4], 1, false);

  piano_music = game.add.audio("piano_music");

  game.world.setBounds(0, 0, 10000, 388);

  initialize_player();
  keys.init();

  onTweenComplete();
}
function update() {
  player.move();
}
function render() {

  if (keys.upKey.isDown) {
    // RAFI CAN PLAY music
    if (rafi.x > 450 && rafi.x < 510) {
      if (!piano_music.isPlaying) {
        piano_music.play();
      }
    }

    // RAFI CAN PLAY isaac
    if (rafi.x >= 90 && rafi.x <= 220) {
      console.log('can launch game');
      if (!isaac_game.isPlaying()) {
        console.log('launch game');
        isaac_game.play();
      }
    }
  }
}
function onTweenComplete() {
  switch (state) {
    case states.init:
      /*
       * Rafi is displayed in his room.
       */
      rafi.x = 150;
      rafi.y = 220;
      break;
  }
}
