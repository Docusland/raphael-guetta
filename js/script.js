jQuery(document).ready(function ($) {

    function displayInContainer(path) {
        if (path) {
            console.log("Loading " + path);
            $.get(path)
                .success(function (data, textStatus, jqXHR) {
                    $('#content').html(jqXHR.responseText);
                })
                .fail(function (data) {
                    console.error("FAILURE : " + JSON.stringify(data));
                });
        } else {
            console.error("AAAAAH MAIS QUE FAIS-TU !?");
        }
    }

    // AJAXify all anchors that have the `data-ajax-url` attribute
    var ajaxLinks = $("a[data-ajax-url]");
    ajaxLinks.click(
        function () {
            var url = $(this).attr("data-ajax-url");
            ajaxLinks.removeClass('active');
            $(this).addClass('active');
            displayInContainer(url);
        }
    );

});


// Chrismas Stupid Code
var d = new Date();
if(d.getMonth() == 11) {
	//We are in december. Let's rock !
	console.log('   Joyeux Noël !!!');
	console.log('          {_}');
	console.log('          / \\');

	console.log('        /_____\\');
	console.log('      {`_______`}');
	console.log('       // . . \\\\');
	console.log('      (/(__7__)\\)');
	console.log('      |         |');
	console.log('      /\\       /\\');
	console.log('     /  ".   ."  \\');
	console.log('    /_/   `"`   \\_\\');
	console.log('   {__}###[_]###{__}');
	console.log('   (_/\\_________/\\_)');
	console.log('       |___|___|');
	console.log('        |--|--|');
	console.log('       (__)`(__)');

}
